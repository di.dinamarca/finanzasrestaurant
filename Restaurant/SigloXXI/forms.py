from django import forms
from django.forms import ModelForm
from django.forms import widgets
from django.forms.models import ModelChoiceField
from django.forms.widgets import Widget
from . models import Boleta

"""
class BoletaForm(forms.ModelForm):
    class Meta:
        model = Boleta
        fields = ['id', 'fecha_emision' , 'producto', 'cantidad', 'precioUnitario', 'precioSubtotal', 'iva', 'totalBoleta']
        labels = {
            'id' : 'Id',
            'fecha_emision' : 'FechaEmision',
            'producto' : 'Producto',
            'cantidad' : 'Cantidad',
            'precioUnitario' : 'PrecioUnitario',
            'precioSubtotal' : 'PrecioSubtotal',
            'total' : 'total',

        }
        widgets={
            'id' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Ingrese numero Boleta',
                    'id' : 'id'
                }
            ),
            'fecha_emision' : forms.DateTimeField(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Ingrese Fecha',
                    'fecha_emision' : 'FechaEmision'
               }
            ),
            'producto' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'ingrese Productos',
                    'producto' : 'Producto'
                  }
            ),
            'cantidad' : forms.TextInput(
                attrs={
                'class' : 'form-control',
                'placeholder' : 'Cantidad',
                'cantidad' : 'Cantidad' 
                }           

                
            ),

            'precioUnitario' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Precio Unidad',
                    'precioUnitario' : 'PrecioUnitario'
                }
            ),
            'precioSubtotal' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Precio Subtotal',
                    'precioSubtotal' : 'PrecioSubtotal'
                }
            ),
            'total' : forms.TextInput(
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Total',
                    'total' : 'Total'
                }
            )
        
                
        }
"""
