from unicodedata import name
from django.shortcuts import render

def index(request):
    return render (request, 'index.html')
# Create your views here.

def emitirBoleta(request):
    return render (request, 'emitirBoleta.html')

def emitirBoletaInsumos(request):
    return render(request, 'emitirBoletaInsumos.html')

def verBoletas(request):
    return render (request, 'verBoletas.html')

def verBoletasInsumos(request):
    return render(request, 'verBoletasInsumos.html')

def eliminarBoletaVenta(request):
    return render(request, 'eliminarBoletaVenta.html')

def eliminarBoletaInsumo(request):
    return render(request, 'eliminarBoletaInsumo.html')

def registroHistoricoVentas(request):
    return render(request, 'registroHistoricoVentas.html')

def registroHistoricoCompras(request):
    return render(request, 'registroHistoricoCompras.html')

def detallesRegistro(request):
    return render(request, 'detallesRegistro.html')

