from django.db import models

# Create your models here.

class Boletainsumos(models.Model):
    bolIns_id = models.BigAutoField( primary_key=True)  # Field name made lowercase.
    bolIns_empresa = models.TextField( blank=True, null=True)  # Field name made lowercase.
    bolIns_fecha = models.DateTimeField( blank=True, null=True)  # Field name made lowercase.
    bolIns_cant = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolIns_descripcion = models.TextField( blank=True, null=True)  # Field name made lowercase.
    bolIns_total = models.IntegerField( blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BoletaInsumos'


class Boletaventas(models.Model):
    bolven_id = models.BigAutoField(primary_key=True)  # Field name made lowercase.
    bolven_fecha = models.DateTimeField( blank=True, null=True)  # Field name made lowercase.
    bolven_productoconsumido = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolven_cant = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolven_preciounitario = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolven_subtotal = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolven_iva = models.IntegerField( blank=True, null=True)  # Field name made lowercase.
    bolven_totalboleta = models.IntegerField( blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'BoletaVentas'


class Pedidos(models.Model):
    ped_id = models.IntegerField(primary_key=True)
    ped_mesa = models.IntegerField(blank=True, null=True)
    ped_pedido = models.TextField(blank=True, null=True)
    ped_intrucciones_esp = models.TextField( blank=True, null=True)  # Field name made lowercase.
    ped_hora_pedido = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Pedidos'


class Productos(models.Model):
    prod_id = models.BigAutoField(primary_key=True)
    prod_nombre = models.TextField(blank=True, null=True)
    prod_marca = models.TextField(blank=True, null=True)
    prod_descripcion = models.TextField(blank=True, null=True)        
    prod_fecha_ingreso = models.TimeField(blank=True, null=True)      
    prod_cantidad = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Productos'


class Proveedores(models.Model):
    prov_id = models.BigAutoField(primary_key=True)
    prov_nombre = models.TextField(blank=True, null=True)
    prov_contacto = models.IntegerField(blank=True, null=True)
    prov_direccion = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Proveedores'


class Recetas(models.Model):
    rec_id = models.BigAutoField(primary_key=True)
    rec_nombre = models.TextField(blank=True, null=True)
    rec_ingrediente = models.TextField(blank=True, null=True)
    rec_preparacion = models.TextField(blank=True, null=True)
    rec_comentario = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Recetas'


class Registrocliente(models.Model):
    reg_id = models.BigAutoField(primary_key=True)
    reg_nombre = models.TextField(blank=True, null=True)
    reg_usuario = models.TextField(blank=True, null=True)
    reg_password = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'RegistroCliente'


class Reserva(models.Model):
    res_id = models.BigAutoField(primary_key=True)
    res_mesa = models.IntegerField(blank=True, null=True)
    res_cantpersonas = models.IntegerField(blank=True, null=True)  # Field name made lowercase.
    res_fecha_solicitud = models.DateField(blank=True, null=True)
    res_fecha_ingreso = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Reserva'


class Trabajadores(models.Model):
    trab_id = models.BigAutoField(primary_key=True)
    trab_fono = models.IntegerField(blank=True, null=True)
    trab_correo = models.TextField(blank=True, null=True)
    trab_password = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Trabajadores'
