from pydoc import describe
from rest_framework import serializers
from .models import *
from django.forms import ModelForm


class ProductosSerializers(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = ('prod_id', 'prod_nombre','prod_marca','prod_descripcion','prod_fecha_ingreso',
        'prod_cantidad')