
from calendar import c
from csv import list_dialects
from email.policy import default
from functools import partial
import imp
import json
from pickle import LIST
import re
from urllib import request, response
from django.shortcuts import render, redirect
from django.contrib import auth
from .serializer import *
from .models import * 
from rest_framework.response import Response
from Restorant import serializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from datetime import date, datetime
from django.db.models import F


# Create your views here.


def login(request):
    return render(request, 'login.html')

def inventario(request):
    return render(request, 'inventario.html')

def datosGenerales(request):
    return render(request, 'datosGenerales.html')

def menuAdm(request):
    return render(request, 'menuAdm.html')

def ingresarProd(request):
    return render(request, 'ingresarProd.html')


@api_view(["POST"])
def ingresarProducto(request):
    if request.method == 'POST':
        request.data["prod_fecha_ingreso"] = datetime.now()
        producto_serializer = ProductosSerializers(data=request.data)
        if producto_serializer.is_valid():
            producto_serializer.save()
            print ('hace todo bien')
            return Response("agregado correctamente")
        return Response("no se agrego")

@api_view(["GET"])
def listaProducto(request):
    if request.method == 'GET':
        Prod = Productos.objects.values()
        return Response(Prod)

""" @api_view(["GET"])
def bolInsumo(request):
    if request.method == 'GET':
        bolInsumo = Boletainsumos.objects.values()
        return Response(bolInsumo) """

#finanazas
def index(request):
    return render (request, 'index.html')

    
# Create your views here.

def emitirBoleta(request):
    return render (request, 'emitirBoleta.html')

def emitirBoletaInsumos(request):
    return render(request, 'emitirBoletaInsumos.html')

def verBoletas(request):
    return render (request, 'verBoletas.html')

def verBoletasInsumos(request):
    return render(request, 'verBoletasInsumos.html')

def eliminarBoletaVenta(request):
    return render(request, 'eliminarBoletaVenta.html')

def eliminarBoletaInsumo(request):
    return render(request, 'eliminarBoletaInsumo.html')

def registroHistoricoVentas(request):
    return render(request, 'registroHistoricoVentas.html')

def registroHistoricoCompras(request):
    return render(request, 'registroHistoricoCompras.html')

def detallesRegistro(request):
    return render(request, 'detallesRegistro.html')

def create_BoletaVentas(request):
    bolVenta = Boletaventas(bolven_id=request.POST['id'], 
                            bolven_fecha=request.POST['fecha'], 
                            bolven_productoconsumido=request.POST['productoConsumido'], 
                            bolven_cant=request.POST['cantidad'], 
                            bolven_preciounitario=request.POST['precioUnitario'], 
                            bolven_subtotal=request.POST['Subtotal'], 
                            bolven_iva=request.POST['IVA'], 
                            bolven_totalboleta= request.POST['total'])
    bolVenta.save()
    return redirect('/emitirBoleta/')