
from django.urls import path
from .views import *
from Restorant import views
from .views import emitirBoleta, index, emitirBoletaInsumos, verBoletas, verBoletasInsumos, eliminarBoletaVenta, eliminarBoletaInsumo, registroHistoricoVentas, registroHistoricoCompras, detallesRegistro


urlpatterns = [
    path('inventario/', views.inventario),
    path('login/', views.login),
    path('datosGenerales/', views.datosGenerales),
    path('menuAdm/',  views.menuAdm),
    path('ingresarProd/', views.ingresarProd),

    # axios
    path('listaProducto/', views.listaProducto),
    path('ingresarProducto/', views.ingresarProducto),

    #finanzas
    path('index/',views.index),
    path('emitirBoleta/', views.emitirBoleta, name='emitirBoleta'),
    path('emitirBoletaInsumos/', views.emitirBoletaInsumos, name='emitirBoletaInsumos'),
    path('verBoletas/', views.verBoletas, name='verBoletas'),
    path('verBoletasInsumos/', views.verBoletasInsumos, name='verBoletasInsumos'),
    path('eliminarBoletaVenta/', views.eliminarBoletaVenta, name='eliminarBoletaVenta'),
    path('eliminarBoletaInsumo/', views.eliminarBoletaInsumo, name='eliminarBoletaInsumo'),
    path('registroHistoricoVentas/', views.registroHistoricoVentas),
    path('registroHistoricoCompras/', views.registroHistoricoCompras, name='registroHistoricoCompras'),
    path('detallesRegistro/', views.detallesRegistro, name='detallesRegistro'),

]
